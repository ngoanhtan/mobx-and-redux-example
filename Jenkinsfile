
String MINOR_RELEASE = 'minor'
String MAJOR_RELEASE = 'major'
String PATCH_RELEASE = 'patch'
def releaseType 
def currentVersion 
def releaseVersion 
def nextVersion 
def nextMinorVersion 
def releaseBranch 
def nextMinorBranch
pipeline {
    environment {
        profiles = calculateProfiles()
        projectDeployVersion = calculateProjectDeployVersion()
        GRADLE_USER_HOME = "${WORKSPACE}/.gradle"
    }

    agent any

    stages {
        stage('Initialize') {
            steps {
                script {
                    deleteDir()
                }
            }
        }
        stage('checkout') {
            steps {
                checkout scm
                script {
                    releaseType = getReleaseType()
                    currentVersion = readCurrentVersion()
                    releaseVersion = calculateReleaseVersion()
                    nextVersion = calculateNextVersion()
                    nextMinorVersion = calculateNextMinorVersion()
                    releaseBranch = String.format('release/%s', calculateReleaseBranch())
                    nextMinorBranch = String.format('release/%s', calculateNextMinorBranch())
                }
            }
        }
        stage('Create Release Branch') {
            steps {
                script {
                    if (releaseType == MAJOR_RELEASE) {
                        sh """
                            git checkout -b ${releaseBranch}
                        """
                    }
                    else {
                        sh """
                            git checkout ${releaseBranch} && git merge --commit --no-edit origin/${BRANCH}
                        """
                    }
                }
            }
        }
        stage('Create next minor release branch') {
            steps {
                script {
                    if (releaseType == MAJOR_RELEASE || releaseType == MINOR_RELEASE) {
                        sh """
                            git checkout -b ${nextMinorBranch}
                        """
                        def pom = readMavenPom file: 'pom.xml'
                        pom.version = "${nextMinorVersion}"
                        writeMavenPom model: pom
                        sh """
                            git add pom.xml
                            git commit -m "change pom version to ${nextMinorVersion}"
                        """
                    }
                }
            }
        }
        stage('Modify pom version') {
            steps {
                script {
                    sh """
                        git checkout ${releaseBranch}
                    """
                    def pom = readMavenPom file: 'pom.xml'
                    pom.version = "${nextVersion}"
                    writeMavenPom model: pom

                    sh """
                        git add pom.xml
                        git commit -m "change pom version to ${nextVersion}"
                    """

                    if (releaseType == MAJOR_RELEASE) {
                        sh """
                            git checkout ${BRANCH}
                        """

                        pom = readMavenPom file: 'pom.xml'
                        String[] split = pom.version.split("\\.")
                        int parseInt = Integer.parseInt(split[0]) + 1
                        pom.version = String.format('%s.%s.%s', parseInt, split[1], split[2] )
                        writeMavenPom model: pom
                        sh """
                            git add pom.xml
                            git commit -m "change pom version to ${pom.version}"
                        """
                    }
                }
            }
        }
        stage('Push Release Branch') {
            steps {
                script {
                    sh """
                        git push origin ${releaseBranch}:${releaseBranch}
                    """
                    if (releaseType == MAJOR_RELEASE || releaseType == MINOR_RELEASE) {
                        sh """
                            git push origin ${BRANCH}:${BRANCH}
                            git push origin ${nextMinorBranch}:${nextMinorBranch}
                        """
                    }

                    sh """
                        git checkout ${releaseBranch}
                        git tag ${releaseVersion.split('-')[0]}
                        git push --tags
                    """
                }
            }
        }
        

        stage('Set Release Version') {
            steps {
                echo "Setting version version: ${releaseVersion}"
                sh 'echo 1'
            }
        }
    }
    post {
        always {
            cleanWs cleanWhenFailure: false, cleanWhenUnstable: false
        }
    }
}

boolean shouldNotifyDevelopmentTeam() {
    return env.BUILD_TYPE == 'NIGHTLY' || env.BUILD_TYPE == 'RELEASE'
}

String calculateProjectDeployVersion() {
    String projectDeployVersion = ''
    if (env.BUILD_TYPE == 'NIGHTLY') {
        projectDeployVersion = 'uaa_example_nightly'
    }
    if (env.BUILD_TYPE == 'RELEASE') {
        projectDeployVersion = calculateReleaseVersion()
    }
    return projectDeployVersion
}

def String readCurrentVersion() {
    //return '4.3.1-SNAPSHOT'
    return readMavenPom(file: 'pom.xml').version
}

def String calculateReleaseVersion() {
    String version = readCurrentVersion().replaceAll('-SNAPSHOT', "-build${String.format('%05d', Integer.parseInt(env.BUILD_NUMBER))}")
    if (params.VERSION) {
        version = "${params.VERSION}-build${String.format('%05d', Integer.parseInt(env.BUILD_NUMBER))}"
    }
    return version
}

def String calculateNextVersion() {
    String nextVersion = calculateReleaseVersion().replaceAll("-build${String.format('%05d', Integer.parseInt(env.BUILD_NUMBER))}", '')
    String[] split = nextVersion.split("\\.")
    int parseInt = Integer.parseInt(split[2]) + 1
    nextVersion = String.format('%s.%s.%s', split[0], split[1], parseInt)

    return String.format('%s-SNAPSHOT', nextVersion)
}

def String calculateNextMinorVersion() {
    String nextVersion = calculateReleaseVersion().replaceAll('-SNAPSHOT', '')
    String[] split = nextVersion.split("\\.")
    int parseInt = Integer.parseInt(split[1]) + 1
    nextVersion = String.format('%s.%s.0', split[0], parseInt)

    return String.format('%s-SNAPSHOT', nextVersion)
}

def String calculateProfiles() {
    String profilesParameter = env.PROFILES
    if (profilesParameter) {
        return String.format('%s', profilesParameter)
    }
    return ' '
}

def String calculateReleaseBranch() {
    String version = calculateReleaseVersion()
    String[] split = version.split("\\.")
    return String.format('%s.%s', split[0], split[1])
}

def String calculateNextMinorBranch() {
    String version = calculateNextMinorVersion()
    String[] split = version.split("\\.")
    return String.format('%s.%s', split[0], split[1])
}

def int getReleaseType() {
    String version = calculateReleaseVersion().replaceAll("-build${String.format('%05d', Integer.parseInt(env.BUILD_NUMBER))}", '')
    String[] split = version.split("\\.")
    if (params.VERSION) {
        split = params.VERSION.split("\\.")
    }
    echo "split ${split}"
    if (split[1] == '0' && split[2] == '0') {
        return 'major'
    }
    if (split[1] != '0' && split[2] == '0') {
        return 'minor'
    }

    return 'patch'
}

def EMAIL_RECIPIENTS = 'Tuan.Minh.Do@mgm-tp.com,Tomas.Kupka@mgm-tp.com,Tan.Van.Anh.Ngo@mgm-tp.com,Nguyen.Cao.Dang@mgm-tp.com,Tuan.Thanh.Tran@mgm-tp.com,Quyen.Hong.Le@mgm-tp.com'

def BASE_PATH = 'UAA/Authentication-Authorization/DSL-Jobs'
def RELEASE = "${BASE_PATH}/release"

pipelineJob("${RELEASE}") {
    description()
    keepDependencies(false)
    parameters {
        stringParam('BRANCH', 'master', '')
        stringParam('VERSION', '', 'Specify Release version. If nothing is specified then POM version is used (-SNAPSHOT is removed)')
        stringParam('NEXT_VERSION', '', 'Specify Release version. If nothing is specified then POM version is used (-SNAPSHOT is removed) and fix version is increased')
    }
    environmentVariables {
        env('BUILD_TYPE', 'RELEASE')
        env('NODE', 'linux-1||linux-2')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    definition {
        cpsScm {
            scm {
                git {
                    browser { }
                    branch('${BRANCH}')
                    remote {
                        credentials('a12-ci')
                        url('ssh://git@bitbucket.mgm-tp.com:7999/a12/uaa.git')
                    }
                }
                scriptPath('jenkinsfile')
                lightweight(false)
            }
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}
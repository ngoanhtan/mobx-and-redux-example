def EMAIL_RECIPIENTS = 'Tuan.Minh.Do@mgm-tp.com,Tomas.Kupka@mgm-tp.com,Tan.Van.Anh.Ngo@mgm-tp.com,Nguyen.Cao.Dang@mgm-tp.com,Tuan.Thanh.Tran@mgm-tp.com,Quyen.Hong.Le@mgm-tp.com'

def BASE_PATH = 'UAA/UAA_User-Management/DSL-Jobs'
def DEPLOYMENT = "${BASE_PATH}/deployment"

pipelineJob("${DEPLOYMENT}") {
    description()
    keepDependencies(false)
    parameters {
        stringParam('BRANCH', 'master', '')
        stringParam('IMAGE_DEPLOY_VERSION', '3.0.0-rc.4', 'Version which need to be deployed')
        choiceParam('DEPLOY_FOLDER', ['uaa-example_3', 'uaa-example_2', 'uaa-example_1','uaa-example_4','uaa-example_5'], '''The folder deployment for uaa-example-application. The deploy folder has 2 parts:
1. 'uaa-example' is kind of prefix we should use this for all folder deployment of uaa-example-application.
2. '3' is major version of deployment. We should keep the latest version of major release. Any other version deploy 3.1, 3.2, 3.2.2 will have to use the same folder deployment.
This folder name will be generated if it's not found in the master branch.
In case of any other deployment minor or patches the folder name wil be used instead of generating.
After successfully deployment the new folder will be push to artifactory.''')
    }
    environmentVariables {
        env('NODE', 'linux-1||linux-2||linux-3||linux-4')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    definition {
        cpsScm {
            scm {
                git {
                    browser { }
                    branch('${BRANCH}')
                    remote {
                        credentials('a12-ci')
                        url('ssh://git@bitbucket.mgm-tp.com:7999/a12/uaa.git')
                    }
                }
                scriptPath('devapps/deployment/Jenkinsfile')
                lightweight(false)
            }
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}
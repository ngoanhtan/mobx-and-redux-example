def EMAIL_RECIPIENTS = 'Tuan.Minh.Do@mgm-tp.com,Tomas.Kupka@mgm-tp.com,Tan.Van.Anh.Ngo@mgm-tp.com,Nguyen.Cao.Dang@mgm-tp.com,Tuan.Thanh.Tran@mgm-tp.com,Quyen.Hong.Le@mgm-tp.com'

def BASE_PATH = 'UAA/Authentication-Authorization'
def PR_BUILDER = "${BASE_PATH}/Bitbucket"

organizationFolder("${PR_BUILDER}") {
    organizations {
        bitbucket {
            serverUrl('https://bitbucket.mgm-tp.com')
            credentialsId('a12ci-artifactory')
            repoOwner('A12')
            traits {
                sourceWildcardFilter {
                    includes('uaa')
                    excludes('')
                }
                bitbucketBranchDiscovery {
                    strategyId(1)
                }
                bitbucketPullRequestDiscovery {
                    strategyId(1)
                }
                headWildcardFilter {
                    includes('*')
                    excludes('bugfix* feature* A12UAA*')
                }
                cleanBeforeCheckoutTrait {
                    extension { }
                }
                wipeWorkspaceTrait()
            }
        }
    }
    buildStrategies {
        buildChangeRequests {
            ignoreTargetOnlyChanges(false)
            ignoreUntrustedChanges(false)
        }
        buildRegularBranches()
        buildTags {
            atLeastDays('')
            atMostDays('7')
        }
    }
    projectFactories {
        workflowMultiBranchProjectFactory {
            scriptPath('jenkinsfile')
        }
    }
    triggers {
        periodicFolderTrigger {
            interval('4h')
        }
    }
    orphanedItemStrategy {
        discardOldItems {
            daysToKeep(-1)
            numToKeep(-1)
        }
        defaultOrphanedItemStrategy {
            pruneDeadBranches(true)
            daysToKeepStr('')
            numToKeepStr('')
        }
    }
    configure {
        def templates = it / 'properties' / 'jenkins.branch.OrganizationChildTriggersProperty' / templates
        templates << 'com.cloudbees.hudson.plugins.folder.computed.PeriodicFolderTrigger' {
            spec('* * * * *')
            interval('300000')
        }
        def traits = it / 'navigators' / 'com.cloudbees.jenkins.plugins.bitbucket.BitbucketSCMNavigator' / traits
        traits << 'com.cloudbees.jenkins.plugins.bitbucket.ForkPullRequestDiscoveryTrait' {
            strategyId(1)
            trust(class: 'com.cloudbees.jenkins.plugins.bitbucket.ForkPullRequestDiscoveryTrait$TrustTeamForks')
        }
    }
}

def FIREFOX_DRIVER_VERSION = '0.28.0'
def CHROME_DRIVER_VERSION = '91.0.4472.101'
def EMAIL_RECIPIENTS = 'Tuan.Minh.Do@mgm-tp.com,Tomas.Kupka@mgm-tp.com,Tan.Van.Anh.Ngo@mgm-tp.com,Nguyen.Cao.Dang@mgm-tp.com,Tuan.Thanh.Tran@mgm-tp.com,Quyen.Hong.Le@mgm-tp.com'

def BASE_PATH = 'UAA/Authentication-Authorization/DSL-Jobs'
def NIGHTLY_JOB_NAME = "${BASE_PATH}/nightly"
def NIGHTLY_RUNNER_NAME = "${BASE_PATH}/nightly-run-all"

pipelineJob("${NIGHTLY_JOB_NAME}") {
    description()
    keepDependencies(false)
    parameters {
        stringParam('BRANCH', 'master', 'Branch name for run CI/CD')
        stringParam('ATLAS_SCANNERMANAGER_KEY', '388d89686906a9f5ee35cd2265798b8fd35df69bda935be70a7a36fbebc6fbba', '')
        stringParam('ATLAS_REPORTER_KEY', '605e0d758e92630588aab3b0087988f03ac11149b2b71f3caa39850bddc842e9', '')
        stringParam('ATLAS_DEFECTDOJO_KEY', '512b685ff426d6d6b68dd8c16a9630161a818841', '')
        stringParam('ATLAS_DROPZONE_KEY', '6f89c86f1da53556bda9a5effdd0d3b0e6031ddc182eba0652b613084cf9c188', '')
        stringParam('FIREFOX_DRIVER_VERSION', "${FIREFOX_DRIVER_VERSION}", '')
        stringParam('CHROME_DRIVER_VERSION', "${CHROME_DRIVER_VERSION}", '')
        stringParam('EMAIL_RECIPIENTS', "${EMAIL_RECIPIENTS}", 'Send email to Tuan.Minh.Do@mgm-tp.com, Tomas.Kupka@mgm-tp.com, Tan.Van.Anh.Ngo@mgm-tp.com, Nguyen.Cao.Dang@mgm-tp.com, Tuan.Thanh.Tran@mgm-tp.com, Quyen.Hong.Le@mgm-tp.com')
    }
    environmentVariables {
        env('BUILD_TYPE', 'NIGHTLY')
        env('NODE', 'linux-1||linux-2')
        env('PROFILES', 'default,performance')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    definition {
        cpsScm {
            scm {
                git {
                    browser { }
                    branch("\${BRANCH}")
                    remote {
                        credentials('a12-ci')
                        url('ssh://git@bitbucket.mgm-tp.com:7999/a12/uaa.git')
                    }
                }
                scriptPath('jenkinsfile')
                lightweight(false)
            }
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}

pipelineJob("${NIGHTLY_RUNNER_NAME}") {
    description()
    keepDependencies(false)
    parameters {
        stringParam('VERSIONS', '3.0,3.1,3.2,3.3', 'List versions that nightly run. These versions seperate by \',\' \n Example: 3.0,3.1,3.2')
        stringParam('EMAIL_RECIPIENTS', "${EMAIL_RECIPIENTS}", 'Send email to Tuan.Minh.Do@mgm-tp.com, Tomas.Kupka@mgm-tp.com, Tan.Van.Anh.Ngo@mgm-tp.com, Nguyen.Cao.Dang@mgm-tp.com, Tuan.Thanh.Tran@mgm-tp.com, Quyen.Hong.Le@mgm-tp.com')
    }
    environmentVariables {
        env('BUILD_TYPE', 'NIGHTLY')
        env('NODE', 'linux-1||linux-2')
        env('PROFILES', 'default,performance')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    properties {
        pipelineTriggers {
            triggers {
                cron {
                    spec('H 21 * * *')
                }
            }
        }
    }
    definition {
        cps {
            sandbox(true)
            script("""pipeline {
    agent any
    stages {
        stage(\'Start\') {
            steps {
                nightlyForAllRelease()
            }
        }
    }
            }

def nightlyForAllRelease() {
    String versionsStr = params.VERSIONS
    String[] versions = versionsStr.split(\',\')
    def branches = [:]
    for (version in versions) {
        build   wait: true,
                propagate: false,
                job: \'${NIGHTLY_JOB_NAME}\',
                parameters: [
                    string(name: \'BRANCH\', value: "release/\${version.trim()}"),
                    string(name: \'EMAIL_RECIPIENTS\', value: "\${params.EMAIL_RECIPIENTS}")
                    string(name: \'DEPLOY\', value: 'false')
                ]
    }
    build   wait: true,
        propagate: false,
        job: \'${NIGHTLY_JOB_NAME}\',
        parameters: [
            string(name: \'BRANCH\', value: "master"),
            string(name: \'EMAIL_RECIPIENTS\', value: "\${params.EMAIL_RECIPIENTS}"),
            string(name: \'DEPLOY\', value: 'true')
        ]
}""")
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}

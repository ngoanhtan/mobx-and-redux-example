import common
def FIREFOX_DRIVER_VERSION = '0.28.0'
def CHROME_DRIVER_VERSION = '91.0.4472.101'
def EMAIL_RECIPIENTS = 'Tuan.Minh.Do@mgm-tp.com,Tomas.Kupka@mgm-tp.com,Tan.Van.Anh.Ngo@mgm-tp.com,Nguyen.Cao.Dang@mgm-tp.com,Tuan.Thanh.Tran@mgm-tp.com,Quyen.Hong.Le@mgm-tp.com'

// def BASE_PATH = 'UAA/Authentication-Authorization/DSL-Jobs'
def ATLAS_NIGHTLY_JOB_NAME = "${common.BASE_PATH}/atlas-nightly-cronjob"
def ATLAS_INTEGRATION_JOB_NAME = "${BASE_PATH}/atlas-integration-cronjob"

pipelineJob("${ATLAS_INTEGRATION_JOB_NAME}") {
    description()
    keepDependencies(false)
    parameters {
        stringParam('BRANCH', 'master', 'Branch name for run CI/CD')
        stringParam('ATLAS_SCANNERMANAGER_KEY', 'c6f7d71e6928aec747558ccffdc119a73571cec7810a708803b5b620fb5aed9c', '')
        stringParam('ATLAS_REPORTER_KEY', '943aa4de034deded6546e3595d1575bb703e8da3939f17128ef70bea5dc5720a', '')
        stringParam('ATLAS_DEFECTDOJO_KEY', 'add793f060dd012a36b7c5e52fe24a352ea460d1', '')
        stringParam('ATLAS_DROPZONE_KEY', '59ee922554ab295fecf48cbbb9d287605e22a95b9797fe83e8aea492e9c0c0b0', '')
        stringParam('FIREFOX_DRIVER_VERSION', "${FIREFOX_DRIVER_VERSION}", '')
        stringParam('CHROME_DRIVER_VERSION', "${CHROME_DRIVER_VERSION}", '')
        stringParam('EMAIL_RECIPIENTS', "${EMAIL_RECIPIENTS}", 'Send email to Tuan.Minh.Do@mgm-tp.com, Tomas.Kupka@mgm-tp.com, Tan.Van.Anh.Ngo@mgm-tp.com, Nguyen.Cao.Dang@mgm-tp.com, Tuan.Thanh.Tran@mgm-tp.com, Quyen.Hong.Le@mgm-tp.com')
    }
    environmentVariables {
        env('BUILD_TYPE', 'NIGHTLY')
        env('NODE', 'linux-1||linux-2')
        env('PROFILES', 'default')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    definition {
        cpsScm {
            scm {
                git {
                    browser { }
                    branch('feature/A12UAA-749-implement-new-scan-for-javascript-language')
                    remote {
                        credentials('a12-ci')
                        url('ssh://git@bitbucket.mgm-tp.com:7999/a12/uaa.git')
                    }
                }
                scriptPath('jenkinsfile-atlas-scan')
                lightweight(false)
            }
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}

pipelineJob("${ATLAS_NIGHTLY_JOB_NAME}") {
    description()
    keepDependencies(false)
    parameters {
        stringParam('BRANCH', 'master', 'Branch name for run CI/CD')
        stringParam('ATLAS_SCANNERMANAGER_KEY', 'c6f7d71e6928aec747558ccffdc119a73571cec7810a708803b5b620fb5aed9c', '')
        stringParam('ATLAS_REPORTER_KEY', '943aa4de034deded6546e3595d1575bb703e8da3939f17128ef70bea5dc5720a', '')
        stringParam('ATLAS_DEFECTDOJO_KEY', 'add793f060dd012a36b7c5e52fe24a352ea460d1', '')
        stringParam('ATLAS_DROPZONE_KEY', '59ee922554ab295fecf48cbbb9d287605e22a95b9797fe83e8aea492e9c0c0b0', '')
        stringParam('FIREFOX_DRIVER_VERSION', "${FIREFOX_DRIVER_VERSION}", '')
        stringParam('CHROME_DRIVER_VERSION', "${CHROME_DRIVER_VERSION}", '')
        stringParam('EMAIL_RECIPIENTS', "${EMAIL_RECIPIENTS}", 'Send email to Tuan.Minh.Do@mgm-tp.com, Tomas.Kupka@mgm-tp.com, Tan.Van.Anh.Ngo@mgm-tp.com, Nguyen.Cao.Dang@mgm-tp.com, Tuan.Thanh.Tran@mgm-tp.com, Quyen.Hong.Le@mgm-tp.com')
    }
    environmentVariables {
        env('BUILD_TYPE', 'NIGHTLY')
        env('NODE', 'linux-1||linux-2')
        env('PROFILES', 'default')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    properties {
        pipelineTriggers {
            triggers {
                cron {
                    spec('H 21 * * *')
                }
            }
        }
    }
    definition {
        cpsScm {
            scm {
                git {
                    browser { }
                    branch('*/master')
                    remote {
                        credentials('a12-ci')
                        url('ssh://git@bitbucket.mgm-tp.com:7999/a12/uaa.git')
                    }
                }
                scriptPath('jenkinsfile-atlas-scan')
                lightweight(false)
            }
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}
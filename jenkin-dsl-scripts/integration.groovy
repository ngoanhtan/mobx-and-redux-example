def FIREFOX_DRIVER_VERSION = '0.28.0'
def CHROME_DRIVER_VERSION = '91.0.4472.101'
def EMAIL_RECIPIENTS = 'Tuan.Minh.Do@mgm-tp.com,Tomas.Kupka@mgm-tp.com,Tan.Van.Anh.Ngo@mgm-tp.com,Nguyen.Cao.Dang@mgm-tp.com,Tuan.Thanh.Tran@mgm-tp.com,Quyen.Hong.Le@mgm-tp.com'

def BASE_PATH = 'UAA/Authentication-Authorization/DSL-Jobs'
def INTEGRATE_JOB_NAME = "${BASE_PATH}/release-integration"
def INTEGRATE_RUNNER_NAME = "${BASE_PATH}/release-integration-run-all"

pipelineJob("${INTEGRATE_JOB_NAME}") {
    description()
    keepDependencies(false)
    parameters {
        stringParam('BRANCH', 'release/3.11', 'Branch name for the release')
        stringParam('SRC_BRANCH', 'release/3.0', "Branch integrated into \$BRANCH")
        stringParam('EMAIL_RECIPIENTS', "${EMAIL_RECIPIENTS}", 'Send email to Tuan.Minh.Do@mgm-tp.com, Tomas.Kupka@mgm-tp.com, Tan.Van.Anh.Ngo@mgm-tp.com, Nguyen.Cao.Dang@mgm-tp.com, Tuan.Thanh.Tran@mgm-tp.com, Quyen.Hong.Le@mgm-tp.com')
    }
    environmentVariables {
        env('BUILD_TYPE', 'RELEASE')
        env('NODE', 'linux-1||linux-2||linux-3||linux-4')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    definition {
        cpsScm {
            scm {
                git {
                    browser { }
                    branch("\${BRANCH}")
                    remote {
                        credentials('a12-ci')
                        url('ssh://git@bitbucket.mgm-tp.com:7999/a12/uaa.git')
                    }
                }
                scriptPath('release_integration')
                lightweight(false)
            }
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}

pipelineJob("${INTEGRATE_RUNNER_NAME}") {
    description()
    keepDependencies(false)
    parameters {
        stringParam('VERSIONS', '3.0,3.1,3.2', 'List versions that nightly run. These versions seperate by \',\' \n Example: 3.0,3.1,3.2')
        stringParam('EMAIL_RECIPIENTS', "${EMAIL_RECIPIENTS}", 'Send email to Tuan.Minh.Do@mgm-tp.com, Tomas.Kupka@mgm-tp.com, Tan.Van.Anh.Ngo@mgm-tp.com, Nguyen.Cao.Dang@mgm-tp.com, Tuan.Thanh.Tran@mgm-tp.com, Quyen.Hong.Le@mgm-tp.com')
    }
    environmentVariables {
        env('BUILD_TYPE', 'NIGHTLY')
        env('NODE', 'linux-1||linux-2')
        env('PROFILES', 'default,performance')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    properties {
        pipelineTriggers {
            triggers {
                cron {
                    spec('H 20 * * *')
                }
            }
        }
    }
    definition {
        cps {
            sandbox(true)
            script("""pipeline {
    agent any
    parameters {
        string(defaultValue: \'3.0,3.1,3.2\', description: \'List versions that release integration run. These versions seperate by \\\',\\\' \\n Example: 3.0,3.1,3.2\', name: \'VERSIONS\')
    }
    stages {
        stage(\'Start\') {
            steps {
                releaseIntegration()
            }
        }
    }
            }

def releaseIntegration() {
    String ordered_versions = params.VERSIONS
    String[] versions = ordered_versions.split(\',\').sort(false)
    println("Sorted version list: \${versions}")
    for (int i = 0; i < versions.size() - 1; i++) {
        String src_branch = "release/\${versions[i].trim()}"
        String target_branch = "release/\${versions[i+1].trim()}"
        println("\${src_branch} merge to \${target_branch}")
        build   wait: true,
                propagate: false,
                job: \'${INTEGRATE_JOB_NAME}\',
                parameters: [
                    string(name: \'BRANCH\', value: "\${target_branch}"),
                    string(name: \'SRC_BRANCH\', value: "\${src_branch}"),
                    string(name: \'EMAIL_RECIPIENTS\', value: "\${params.EMAIL_RECIPIENTS}")
                ]
    }
    println(("release/\${versions.last().trim()} merge to master"))
    build   wait: true,
            propagate: false,
            job: \'${INTEGRATE_JOB_NAME}\',
            parameters: [
                string(name: \'BRANCH\', value: "master"),
                string(name: \'SRC_BRANCH\', value: "\${versions.last().trim()}"),
                string(name: \'EMAIL_RECIPIENTS\', value: "\${params.EMAIL_RECIPIENTS}")
            ]
}""")
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}
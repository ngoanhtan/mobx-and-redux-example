def ATLAS_SCANNERMANAGER_KEY = '388d89686906a9f5ee35cd2265798b8fd35df69bda935be70a7a36fbebc6fbba'
def ATLAS_REPORTER_KEY = '605e0d758e92630588aab3b0087988f03ac11149b2b71f3caa39850bddc842e9'
def ATLAS_DEFECTDOJO_KEY = '512b685ff426d6d6b68dd8c16a9630161a818841'
def ATLAS_DROPZONE_KEY = '6f89c86f1da53556bda9a5effdd0d3b0e6031ddc182eba0652b613084cf9c188'
def FIREFOX_DRIVER_VERSION = '0.28.0'
def CHROME_DRIVER_VERSION = '91.0.4472.101'
def EMAIL_RECIPIENTS = 'Tuan.Minh.Do@mgm-tp.com,Tomas.Kupka@mgm-tp.com,Tan.Van.Anh.Ngo@mgm-tp.com,Nguyen.Cao.Dang@mgm-tp.com,Tuan.Thanh.Tran@mgm-tp.com,Quyen.Hong.Le@mgm-tp.com'
def EMAIL_RECIPIENTS_TEST = 'Tan.Van.Anh.Ngo@mgm-tp.com'

pipelineJob('UAA/Authentication-Authorization/DSL-Jobs/integrationDSL') {
    description()
    keepDependencies(false)
    parameters {
        stringParam('BRANCH', 'release/3.11', 'Branch name for the release')
        stringParam('SRC_BRANCH', 'release/3.0', "Branch integrated into \$BRANCH")
        stringParam('EMAIL_RECIPIENTS', "${EMAIL_RECIPIENTS_TEST}", 'Send email to Tuan.Minh.Do@mgm-tp.com, Tomas.Kupka@mgm-tp.com, Tan.Van.Anh.Ngo@mgm-tp.com, Nguyen.Cao.Dang@mgm-tp.com, Tuan.Thanh.Tran@mgm-tp.com, Quyen.Hong.Le@mgm-tp.com')
    }
    environmentVariables {
        env('BUILD_TYPE', 'RELEASE')
        env('NODE', 'linux-1||linux-2||linux-3||linux-4')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    definition {
        cpsScm {
            scm {
                git {
                    browser { }
                    branch("\${BRANCH}")
                    remote {
                        credentials('a12-ci')
                        url('ssh://git@bitbucket.mgm-tp.com:7999/a12/uaa.git')
                    }
                }
                scriptPath('release_integration')
                lightweight(false)
            }
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}

pipelineJob('UAA/Authentication-Authorization/DSL-Jobs/nightlyDSL') {
    description()
    keepDependencies(false)
    parameters {
        stringParam('BRANCH', 'master', 'Branch name for run CI/CD')
        stringParam('ATLAS_SCANNERMANAGER_KEY', "${ATLAS_SCANNERMANAGER_KEY}", '')
        stringParam('ATLAS_REPORTER_KEY', "${ATLAS_REPORTER_KEY}", '')
        stringParam('ATLAS_DEFECTDOJO_KEY', "${ATLAS_DEFECTDOJO_KEY}", '')
        stringParam('ATLAS_DROPZONE_KEY', "${ATLAS_DROPZONE_KEY}", '')
        stringParam('FIREFOX_DRIVER_VERSION', "${FIREFOX_DRIVER_VERSION}", '')
        stringParam('CHROME_DRIVER_VERSION', "${CHROME_DRIVER_VERSION}", '')
        stringParam('EMAIL_RECIPIENTS', "${EMAIL_RECIPIENTS_TEST}", 'Send email to Tuan.Minh.Do@mgm-tp.com, Tomas.Kupka@mgm-tp.com, Tan.Van.Anh.Ngo@mgm-tp.com, Nguyen.Cao.Dang@mgm-tp.com, Tuan.Thanh.Tran@mgm-tp.com, Quyen.Hong.Le@mgm-tp.com')
    }
    environmentVariables {
        env('BUILD_TYPE', 'NIGHTLY')
        env('NODE', 'linux-1||linux-2')
        env('PROFILES', 'default,performance')
        groovy()
        loadFilesFromMaster(false)
        keepSystemVariables(true)
        keepBuildVariables(true)
        overrideBuildParameters(false)
    }
    definition {
        cpsScm {
            scm {
                git {
                    browser { }
                    branch("\${BRANCH}")
                    remote {
                        credentials('a12-ci')
                        url('ssh://git@bitbucket.mgm-tp.com:7999/a12/uaa.git')
                    }
                }
                scriptPath('jenkinsfile')
                lightweight(false)
            }
        }
    }
    disabled(false)
    logRotator {
        daysToKeep(-1)
        numToKeep(10)
        artifactDaysToKeep(-1)
        artifactNumToKeep(-1)
    }
}
